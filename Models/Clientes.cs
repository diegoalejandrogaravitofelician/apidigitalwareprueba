﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DigitalWarePruebaApiREST.Models
{
    public class Clientes
    {
        [Key]
        public int iDCliente { get; set; }
        public String  NombreCliente { get; set; }
        public String EdadCliente { get; set; }
    }
}
