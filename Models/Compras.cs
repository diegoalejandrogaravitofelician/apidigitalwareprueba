﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DigitalWarePruebaApiREST.Models
{
    public class Compras
    {
        [Key]
        public int IDCompra { get; set; }

        public int iDProducto { get; set; }

        public int IDCliente { get; set; }

        public String FechaCompra { get; set; }
    }
}
