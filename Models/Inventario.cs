﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DigitalWarePruebaApiREST.Models
{
    public class Inventario
    {
        [Key]
        public int IDinventario { get; set; }
        public int IDproducto { get; set; }
        public String Cantidad { get; set; }
    }
}
