﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DigitalWarePruebaApiREST.Models
{
    public class Producto
    {   
        [Key]
        public int IDproducto { get; set; }

        public String NombreProducto { get; set; }

        public String PrecioProducto { get; set; }
    }
}
