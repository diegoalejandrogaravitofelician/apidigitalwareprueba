﻿using DigitalWarePruebaApiREST.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DigitalWarePruebaApiREST.Context
{
    public class AppBDContext : DbContext
    {
        public AppBDContext(DbContextOptions<AppBDContext> options) : base(options)
        {
        }
        public DbSet<Clientes> Clientes { get; set; }

        public DbSet<Producto> Producto { get; set; }

        public DbSet<Inventario> Inventario { get; set; }

        public DbSet<Compras> Compras { get; set; }

    }
}
