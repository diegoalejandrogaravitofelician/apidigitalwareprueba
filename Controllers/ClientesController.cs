﻿using DigitalWarePruebaApiREST.Context;
using DigitalWarePruebaApiREST.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace DigitalWarePruebaApiREST.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ClientesController : ControllerBase
    {

        private readonly AppBDContext context;

        public ClientesController(AppBDContext context)
        {
            this.context = context; 
        }
        // GET: api/<ClientesController>
        [HttpGet]
        public  ActionResult Get()
        {
            try
            {
                return Ok(context.Clientes.ToList());
            }
            catch(Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        // GET api/<ClientesController>/5
        [HttpGet("{id}",Name = "GetCliente")]
        public ActionResult Get(int id)
        {
            try
            {
                var idCliente = context.Clientes.FirstOrDefault(cliente => cliente.iDCliente == id);

                return Ok(idCliente);

            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
           
        }

        // POST api/<ClientesController>
        [HttpPost]
        public ActionResult Post([FromBody] Clientes clientes)
        {
            try
            {
                context.Clientes.Add(clientes);
                context.SaveChanges();
                return (CreatedAtAction("GetCliente", new { id = clientes.iDCliente }, clientes));
            } catch (Exception ex)
            {

                return BadRequest(ex.Message);
            
            }

        }

        // PUT api/<ClientesController>/5
        [HttpPut("{id}")]
        public ActionResult Put(int id, [FromBody] Clientes clientes)
        {
            try
            {
                if (clientes.iDCliente == id)
                {
                    context.Entry(clientes).State = EntityState.Modified;
                    context.SaveChanges();
                    return (CreatedAtAction("GetCliente", new { id = clientes.iDCliente }, clientes));
                }
                else 
                {
                    return BadRequest();
                }

            } catch (Exception ex) 
            {
                return BadRequest(ex.Message);
            }

        }

        // DELETE api/<ClientesController>/5
        [HttpDelete("{id}")]
        public ActionResult Delete(int id)
        {
            try
            {
                var cliente = context.Clientes.FirstOrDefault(cliente => cliente.iDCliente == id);

                if (cliente != null)
                {
                    context.Clientes.Remove(cliente);
                    context.SaveChanges();
                    return Ok(id);
                }
                else {
                    return BadRequest();
                }

            } catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }

        }
    }
}
