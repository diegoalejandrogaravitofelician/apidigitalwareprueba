﻿using DigitalWarePruebaApiREST.Context;
using DigitalWarePruebaApiREST.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace DigitalWarePruebaApiREST.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ComprasController : ControllerBase
    {
        private readonly AppBDContext context;

        public ComprasController(AppBDContext context)
        {
            this.context = context;
        }

        // GET: api/<ComprasController>
        [HttpGet]
        public ActionResult<string> Get()
        {
            try
            {
                return Ok(context.Compras.ToList());

            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        // GET api/<ComprasController>/5
        [HttpGet("{id}",Name = "GetCompras")]
        public ActionResult Get(int id)
        {
            try
            {
                var idCompra = context.Compras.FirstOrDefault(Compras => Compras.IDCompra == id);

                return Ok(idCompra);

            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        // POST api/<ComprasController>
        [HttpPost]
        public ActionResult Post([FromBody] Compras compras)
        {
            try
            {
                context.Compras.Add(compras);
                context.SaveChanges();
                return (CreatedAtAction("GetCompras", new { id = compras.IDCompra }, compras));
            }
            catch (Exception ex)
            {

                return BadRequest(ex.Message);

            }
        }

        // PUT api/<ComprasController>/5
        [HttpPut("{id}")]
        public ActionResult Put(int id, [FromBody] Compras compras)
        {
            try
            {
                if (compras.IDCompra == id)
                {
                    context.Entry(compras).State = EntityState.Modified;
                    context.SaveChanges();
                    return (CreatedAtAction("GetCompras", new { id = compras.IDCompra }, compras));
                }
                else
                {
                    return BadRequest();
                }

            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        // DELETE api/<ComprasController>/5
        [HttpDelete("{id}")]
        public ActionResult Delete(int id)
        {
            try
            {
                var idcomnpra = context.Compras.FirstOrDefault(compra => compra.IDCompra == id);

                if (idcomnpra != null)
                {
                    context.Compras.Remove(idcomnpra);
                    context.SaveChanges();
                    return Ok(id);
                }
                else
                {
                    return BadRequest();
                }

            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
