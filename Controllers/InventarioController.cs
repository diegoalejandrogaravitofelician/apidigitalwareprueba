﻿using DigitalWarePruebaApiREST.Context;
using DigitalWarePruebaApiREST.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace DigitalWarePruebaApiREST.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class InventarioController : ControllerBase
    {
        private readonly AppBDContext context;

        public InventarioController(AppBDContext context)
        {
            this.context = context;
        }
        // GET: api/<InventarioController>
        [HttpGet]
        public ActionResult Get()
        {
            try
            {
                return Ok(context.Inventario.ToList());

            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }

        }

        // GET api/<InventarioController>/5
        [HttpGet("{id}", Name = "GetInventario")]
        public ActionResult Get(int id)
        {
            try
            {
                var idinventario = context.Inventario.FirstOrDefault(inventario => inventario.IDinventario == id);

                return Ok(idinventario);

            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        // POST api/<InventarioController>
        [HttpPost]
        public ActionResult  Post([FromBody] Inventario inventario)
        {
            try
            {
                context.Inventario.Add(inventario);
                context.SaveChanges();
                return (CreatedAtAction("GetInventario", new { id = inventario.IDinventario }, inventario));
            }
            catch (Exception ex)
            {

                return BadRequest(ex.Message);

            }
        }

        // PUT api/<InventarioController>/5
        [HttpPut("{id}")]
        public ActionResult Put(int id, [FromBody] Inventario inventario)
        {
            try
            {
                if (inventario.IDinventario == id)
                {
                    context.Entry(inventario).State = EntityState.Modified;
                    context.SaveChanges();
                    return (CreatedAtAction("GetInventario", new { id = inventario.IDproducto }, inventario));
                }
                else
                {
                    return BadRequest();
                }

            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        // DELETE api/<InventarioController>/5
        [HttpDelete("{id}")]
        public ActionResult Delete(int id)
        {
            try
            {
                var inventario = context.Inventario.FirstOrDefault(inventario => inventario.IDinventario == id);

                if (inventario != null)
                {
                    context.Inventario.Remove(inventario);
                    context.SaveChanges();
                    return Ok(id);
                }
                else
                {
                    return BadRequest();
                }

            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
